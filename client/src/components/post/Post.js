import React, { Fragment, useEffect } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import { getPost } from "../../actions/action_post"
import PropTypes from "prop-types"
import PostsItem from "../posts/PostsItem"
import Spinner from "../layout/Spinner"
import CommentForm from "./CommentForm"
import CommentItem from "./CommentItem"
const Post = ({ getPost, post: { post, loading }, auth, match }) => {
	useEffect(() => {
		getPost(match.params.id)
	}, [getPost, match])
	return loading || post === null ? (
		<Spinner />
	) : (
		<Fragment>
			<Link to={`/posts`} className="btn">
				Back To Posts
			</Link>
			<PostsItem post={post} auth={auth} showActions={false} />
			<CommentForm postId={post._id} auth={auth} />

			<div className="comments">
				{post.comments.map(comment => (
					<CommentItem key={comment._id} comment={comment} auth={auth} postId={post._id}/>
				))}
			</div>
		</Fragment>
	)
}

Post.propTypes = {
	getPost: PropTypes.func.isRequired,
	post: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired
}

const mapStatetoProps = (state, ownProps) => {
	return {
		post: state.post,
		auth: state.auth
	}
}

export default connect(
	mapStatetoProps,
	{ getPost }
)(Post)
