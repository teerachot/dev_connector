import React, { useEffect, Fragment } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getCurrentProfile,deleteAccount } from "../../actions/action_profile"
import Spinner from "../layout/Spinner"
import { Link } from "react-router-dom"
import DashboardAction from "./DashboardAction"
import Experiences from "./Experiences"
import Educations from "./Educations"
const Dashboard = ({
	getCurrentProfile,
	auth: { user, loading: loadinguser },
	profile: { profile, loading },
	deleteAccount
}) => {
	useEffect(() => {
		getCurrentProfile()
	}, [getCurrentProfile,loading])

	return loading && profile === null && loadinguser ? (
		<Spinner />
	) : (
		<Fragment>
			<h1 className="large text-primary">Dashboard</h1>
			<p className="lead">
				<i className="fas fa-user" /> Welcome {user && user.name}
			</p>
			{profile !== null ? (
				<Fragment>
					<DashboardAction />
					<Experiences experience={profile.experience} />
					<Educations education={profile.education} />
					<div className="my-2">
						<button className="btn btn-danger" onClick={()=>deleteAccount()}>
							<i className="fas fa-user-minus" />
							Delete My Account
						</button>
					</div>
				</Fragment>
			) : (
				<Fragment>
					<p>You have not yet set up a profile, please add some info</p>{" "}
					<Link to="/create-profile" className="btn btn-primary my-1">
						Create profile{" "}
					</Link>
				</Fragment>
			)}
		</Fragment>
	)
}

Dashboard.propTypes = {
	getCurrentProfile: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	profile: PropTypes.object.isRequired,
	deleteAccount:PropTypes.func.isRequired
}
function mapStateToProps(state, ownProp) {
	return {
		auth: state.auth,
		profile: state.profile
	}
}

export default connect(
	mapStateToProps,
	{ getCurrentProfile,deleteAccount }
)(Dashboard)
