import React from "react"
import { Route, Redirect } from "react-router-dom"
import PropTypes from "prop-types"
import { connect } from "react-redux"

const PrivateRoute = ({
	conponent: Conponent,
	auth: { isAuthenticated, loading },
	...rest
}) => {
	return (
		<Route
			{...rest}
			render={props =>
				!isAuthenticated && !loading ? (
					<Redirect to="/login" />
				) : (
					<Conponent {...props} />
				)
			}
		/>
	)
}

PrivateRoute.propTypes = {
	auth: PropTypes.object.isRequired
}
function mapStateToProps(state, ownProp) {
	return {
		auth: state.auth
	}
}
export default connect(
	mapStateToProps,
	null
)(PrivateRoute)
