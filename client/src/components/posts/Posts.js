import React, { Fragment, useEffect } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getPosts } from "../../actions/action_post"
import PostForm from "./PostForm";
import PostsItem from "./PostsItem"
import Spinner from "../layout/Spinner"

const Posts = ({ getPosts, post: { posts, loading },auth }) => {
	useEffect(() => {
		getPosts()
	}, [getPosts])
	return loading ? (
		<Spinner />
	) : (
		<Fragment>
			<h1 className="large text-primary">Posts</h1>
			<p className="lead">
				<i className="fas fa-user" /> Welcome to the community!
			</p>

			<PostForm />

			<div className="posts">
				{posts.length > 0 ? (
					posts.map(post => <PostsItem key={post._id} post={post} auth={auth} />)
				) : (
					<h4> No Posts found...</h4>
				)}
			</div>
		</Fragment>
	)
}

Posts.propTypes = {
	getPosts: PropTypes.func.isRequired,
	post: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
	return {
        post: state.post,
        auth: state.auth
	}
}

export default connect(
	mapStateToProps,
	{ getPosts }
)(Posts)
