import React, {useState} from 'react'
import PropTypes from 'prop-types'
import { connect } from "react-redux";
import { addPost } from "../../actions/action_post";

const PostForm = ({addPost}) => {
    const [form,setForm] = useState({
        text:""
    })
    const {text} = form
    const onChange = ({name,value})=>{
        setForm({...form,[name]:value})
    }

    const onSubmit = (e) =>{
        e.preventDefault()
        addPost(form)
        setForm({text:""})
    }
    return (
        <div className="post-form">
				<div className="bg-primary p">
					<h3>Say Something...</h3>
				</div>
				<form className="form my-1" onSubmit={e=>onSubmit(e)} >
					<textarea
						name="text"
						cols="30"
						rows="5"
						placeholder="Create a post"
                        required
                        onChange={e=>{
                            onChange(e.target)
                        }}
                        value={text}
					/>
					<input type="submit" className="btn btn-dark my-1" />
				</form>
			</div>
    )
}

PostForm.propTypes = {
    addPost:PropTypes.func.isRequired
}

export default connect(null,{addPost})(PostForm)
