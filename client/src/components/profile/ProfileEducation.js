import React, { Fragment } from "react"
import Moment from "react-moment"
import PropTypes from "prop-types"

const ProfileEducation = ({
	edu: {
		degree,
		school,
		location,
		fieldofstudy,
		from,
		to,
		current,
		description
	}
}) => {
	return (
		<Fragment>
			<h3 className="text-dark">{school}</h3>
			<p>
				<Moment format="YYYY/MM/DD">{from}</Moment> -{" "}
				{current ? " Now" : <Moment format="YYYY/MM/DD">{to}</Moment>}
			</p>
			<p>
				<strong>Degree: </strong>
				{degree}
			</p>
			<p>
				<strong>Field Of Study: </strong>
				{fieldofstudy}
			</p>
			<p>
				<strong>Description: </strong>
				{description}
			</p>
		</Fragment>
	)
}

ProfileEducation.propTypes = {
	edu: PropTypes.object.isRequired
}

export default ProfileEducation
