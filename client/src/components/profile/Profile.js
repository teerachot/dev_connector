import React, { useEffect, Fragment } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getProfileById } from "../../actions/action_profile"
import Spinner from "../../components/layout/Spinner"
import ProfileTop from "./ProfileTop"
import ProfileAbout from "./ProfileAbout"
import ProfileExperience from "./ProfileExperience"
import ProfileEducation from "./ProfileEducation"
import ProfileGithub from "./ProfileGithub"
import { Link } from "react-router-dom"

const Profile = ({
	getProfileById,
	profile: { loading, profile },
	auth,
	match
}) => {
	useEffect(() => {
		getProfileById(match.params.id)
	}, [getProfileById, match])
	return (
		<Fragment>
			{profile === null || loading ? (
				<Spinner />
			) : (
				<Fragment>
					<Link to="/profiles" className="btn btn-light">
						Back To Profiles
					</Link>
					{auth.isAuthenticated &&
						auth.loading === false &&
						auth.user._id === profile.user._id && (
							<Link to="/edit-profile" className="btn btn-drak">
								Edit Profile
							</Link>
						)}
					<div className="profile-grid my-1">
						{/* <!-- Top --> */}
						<ProfileTop profile={profile} />
						{/* About */}
						<ProfileAbout profile={profile} />

						{/* <!-- Experience --> */}
						<div className="profile-exp bg-white p-2">
							<h2 className="text-primary">Experience</h2>
							{profile.experience.length > 0 ? (
								profile.experience.map((exp, index) => (
									<ProfileExperience key={index} exp={exp} />
								))
							) : (
								<h4>no experience credentials</h4>
							)}
						</div>

						{/* <!-- Education --> */}
						<div className="profile-edu bg-white p-2">
							<h2 className="text-primary">Education</h2>
							{profile.education.length > 0 ? (
								profile.education.map((edu, index) => (
									<ProfileEducation key={index} edu={edu} />
								))
							) : (
								<h4>no education credentials</h4>
							)}
						</div>

						{/* <!-- Github --> */}
						{profile.githubusername && (
							<ProfileGithub username={profile.githubusername} />
						)}
					</div>
				</Fragment>
			)}
		</Fragment>
	)
}

Profile.propTypes = {
	getProfileById: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	profile: PropTypes.object.isRequired
}

const mapStateToProps = state => {
	return {
		profile: state.profile,
		auth: state.auth
	}
}

export default connect(
	mapStateToProps,
	{ getProfileById }
)(Profile)
