import React, { Fragment } from "react"
import Moment from "react-moment"
import PropTypes from "prop-types"

const ProfileExperience = ({
	exp: { title, company, location, from, to, current, description }
}) => {
	return (
		<Fragment>
			<h3 className="text-dark">{company}</h3>
			<p>
				<Moment format="YYYY/MM/DD">{from}</Moment> -{" "}
				{current ? " Now" : <Moment format="YYYY/MM/DD">{to}</Moment>}
			</p>
			<p>
				<strong>Position: </strong>
				{title}
			</p>
			<p>
				<strong>Description: </strong>
				{description}
			</p>
		</Fragment>
	)
}

ProfileExperience.propTypes = {
    exp:PropTypes.object.isRequired
}

export default ProfileExperience
