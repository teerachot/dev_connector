import axios from "axios"

const setAuthToken = token => {
	const instance = axios.create({
		baseURL: "http://localhost:5000"
	})
	if (token) {
		instance.defaults.headers.common["Authorization"] = token
	} else {
		delete instance.defaults.headers.common["Authorization"]
	}
}

export default setAuthToken
