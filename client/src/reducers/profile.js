import { PROFILE } from "../actions/type"

const initState = {
	profile: null,
	profiles: [],
	repos: [],
	loading: true,
	error: {}
}

const { GET_GITHUB_USERNAME,GETALL_PROFILE, GET_PROFILE, PROFILE_ERROR ,CLEAR_PROFILE, UPDATE_PROFILE} = PROFILE
export default function(state = initState, action) {
	const { type, payload } = action
	switch (type) {
		case GET_PROFILE:
			return {
				...state,
				profile: payload,
				loading: false
			}
		case GET_GITHUB_USERNAME:
			return {
				...state,
				repos:payload,
				loading:false
			}	
		case GETALL_PROFILE:
			return{
				...state,
				profiles: payload,
				loading: false
			}
		case UPDATE_PROFILE:
			return {
				...state,
				profile:payload,
				loading:false
			}
		case PROFILE_ERROR:
			return {
				...state,
				error: payload,
				loading: false
			}
		case CLEAR_PROFILE:
			return {
				...initState,
				profiles: [],
				profile:null,
				loading:false
			}
		default:
			return {
				...state
			}
	}
}
