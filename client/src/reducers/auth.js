import { REGISTER ,PROFILE} from "../actions/type";

const initState = {
    token:localStorage.getItem('token'),
    isAuthenticated:null,
    loading:true,
    user:null
}


export default function(state = initState,action){
    const {type,payload} = action
    switch(type){
        case REGISTER.USER_LOADED:{
            return {...state,user:payload,loading:false,isAuthenticated:true}
        }
        case REGISTER.REGISTER_SUCCESS:
        case REGISTER.LOGIN_SUCCESS:{
            localStorage.setItem('token',payload.token)
            return {
                ...state,
                ...payload,
                isAuthenticated:true,
                loading:false
            }
        }
        case REGISTER.LOGOUT:
        case REGISTER.LOGIN_FAIL:
        case REGISTER.AUTH_ERROR:
        case PROFILE.DELETE_ACCOUNT:
        case REGISTER.REGISTER_FAIL:{
            localStorage.removeItem('token')
            return {
                ...state,
                ...payload,
                isAuthenticated:false,
                loading:false
            }
        }
        default:
            return state

    }
}

