
import {ALERT} from "../actions/type";
const initState = []

export default function(state=initState,aciton){
    const {type,payload} = aciton
    switch(type){
        case ALERT.SET_ALERT:
            return [...state,payload]
        case ALERT.REMOVE_ALERT:
            return state.filter(alert => alert.id !== payload)
        default:
            return state
    }
}