import { POST } from "../actions/type"

const {
	GET_POSTS,
	POST_ERROR,
	UPDATE_LIKES,
	DELETE_POSTS,
	ADD_POSTS,
	GET_POST,
	ADD_COMMENT,
	DELETE_COMMENT
} = POST

const initState = {
	post: null,
	posts: [],
	loading: true,
	error: {}
}

export default function(state = initState, action) {
	const { type, payload } = action
	switch (type) {
		case GET_POSTS:
			return {
				...state,
				posts: payload,
				loading: false
			}
		case GET_POST:
			return {
				...state,
				post: payload,
				loading: false
			}
		case ADD_POSTS:
			return {
				...state,
				posts: [...state.posts, payload],
				loading: false
			}
		case DELETE_POSTS:
			return {
				...state,
				posts: state.posts.filter(post => post._id !== payload.id),
				loading: false
			}
		case ADD_COMMENT:
			return {
				...state,
				post: { ...state.post, comments: payload },
				loading: false
			}
		case DELETE_COMMENT:
			return {
				...state,
				post: {
					...state.post,
					comments: state.post.comments.filter(
						comment => comment._id !== payload.id
					)
				},
				loading: false
			}
		case UPDATE_LIKES:
			return {
				...state,
				posts: state.posts.map(post =>
					post._id === payload.id ? { ...post, likes: payload.likes } : post
				)
			}
		case POST_ERROR:
			return {
				...state,
				error: payload,
				loading: false
			}
		default:
			return state
	}
}
