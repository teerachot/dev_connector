import { REGISTER, PROFILE } from "./type";
import { setAlert } from "./action_alert";
import axios from "axios";
import setAuthToken from "../utils/setAuthToken";

export const loadUser = () => async dispatch =>{
    if(localStorage.token){
        setAuthToken(localStorage.token)
    }

    try {
        const res = await axios.get('/api/auth')
        dispatch({
            type:REGISTER.USER_LOADED,
            payload:res.data
        })
    } catch (err) {
       dispatch({
           type:REGISTER.AUTH_ERROR,
           payload:err
       })
    }
}


export const register = ({name,email,password}) => async dispatch =>{
	const newUser = {
        name,email,password
    }
    try{
        const config = {
            headers:{
                "Content-Type":"application/json"
            }
        }
        const body = JSON.stringify(newUser)
        const res = await axios.post('/api/user',body,config)
        dispatch({type:REGISTER.REGISTER_SUCCESS,payload:res.data})
    }catch(err){
        const errors = err.response.data.error
        if(errors){
            errors.forEach(error=> dispatch(setAlert(error.msg,"danger")))
        }

        dispatch({type:REGISTER.REGISTER_FAIL})
   
    }
}

export const login = ({email,password}) => async dispatch =>{
	const login = {
       email,password
    }
    try{
        const config = {
            headers:{
                "Content-Type":"application/json"
            }
        }
        const body = JSON.stringify(login)
        const res = await axios.post('/api/auth',body,config)
        dispatch({type:REGISTER.LOGIN_SUCCESS,payload:res.data})
        dispatch(loadUser())
    }catch(err){
        const errors = err.response.data.error
        if(errors){
            errors.forEach(error=> dispatch(setAlert(error.msg,"danger")))
        }
        dispatch({type:REGISTER.LOGIN_FAIL})
    }
}

export const logout = () => async dispatch =>{
    dispatch({type:PROFILE.CLEAR_PROFILE})
    dispatch ({type:REGISTER.LOGOUT})
   
}