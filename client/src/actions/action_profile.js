import axios from "axios";
import { setAlert } from "./action_alert";

import { PROFILE } from "./type";

const {GET_GITHUB_USERNAME,GETALL_PROFILE,GET_PROFILE,PROFILE_ERROR,UPDATE_PROFILE ,CLEAR_PROFILE,DELETE_ACCOUNT} = PROFILE

export const getCurrentProfile = () => async dispatch =>{
    try {
        const res = await axios.get("/api/profile/me")
        dispatch({type: GET_PROFILE,payload:res.data})
    } catch (error) {
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        })
    }
}


export const getProfileById = (user_id) => async dispatch =>{
    try {
        const res = await axios.get(`/api/profile/user/${user_id}`)
        dispatch({type: GET_PROFILE,payload:res.data})
    } catch (error) {
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        })
    }
}

export const getGithubusername = (username) => async dispatch =>{
    try {
        const res = await axios.get(`/api/profile/github/${username}`)
        const data = await JSON.parse(res.data.body)
        dispatch({type: GET_GITHUB_USERNAME,payload:data})
    } catch (error) {
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        })
    }
}

export const getProfiles = () => async dispatch =>{
    dispatch({type:CLEAR_PROFILE})
    try {
        const res = await axios.get("/api/profile")
        dispatch({type: GETALL_PROFILE,payload:res.data})
    } catch (error) {
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        })
    }
}

export const createProfile = (formdata,history,edit=false) => async dispatch =>{
    try {
        let config = {
            headers:{
                "Content-Type":"application/json"
            }
        }

        const res = await axios.post("api/profile/me",formdata,config)
        dispatch({type:GET_PROFILE,payload:res.data})
        dispatch(setAlert(edit? "Profile Update": "Profile Created","success"))
        if(!edit){
            history.push("/dashboard")
        }
    } catch (err) {

        const errors = err.response.data.error
        if(errors){
            errors.forEach(error=> dispatch(setAlert(error.msg,"danger")))
        }

       
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        })
    }
} 


export const addExperience =  (formdata,history)=>async dispatch => {
    try {
        let config = {
            headers:{
                "Content-Type":"application/json"
            }
        }

        const res = await axios.put("api/profile/experience",formdata,config)
        dispatch({type:UPDATE_PROFILE,payload:res.data})
        dispatch(setAlert("Experience Added","success"))
        history.push("/dashboard")
     
    } catch (err) {

        const errors = err.response.data.error
        if(errors){
            errors.forEach(error=> dispatch(setAlert(error.msg,"danger")))
        }

       
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        })
    }
}


export const addEducation =   (formdata,history) =>async dispatch=>{
    try {
        let config = {
            headers:{
                "Content-Type":"application/json"
            }
        }

        const res = await axios.put("api/profile/education",formdata,config)
        dispatch({type:UPDATE_PROFILE,payload:res.data})
        dispatch(setAlert("Education Added","success"))
        history.push("/dashboard")
     
    } catch (err) {

        const errors = err.response.data.error
        if(errors){
            errors.forEach(error=> dispatch(setAlert(error.msg,"danger")))
        }

       
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        })
    }
}


export const deleteExperience = id => async dispatch =>{
    try {
        const res = await axios.delete(`api/profile/experience/${id}`)
        dispatch({type:UPDATE_PROFILE,payload:res.data})
        dispatch(setAlert("Experience Deleted","success"))
    } catch (err) {
        

        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        })
    }
} 

export const deleteEducation = id => async dispatch =>{
    try {
        const res = await axios.delete(`api/profile/education/${id}`)
        dispatch({type:UPDATE_PROFILE,payload:res.data})
        dispatch(setAlert("Education Deleted","success"))
    } catch (err) {
        dispatch({
            type: PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        })
    }
} 


export const deleteAccount = () => async dispatch =>{
    if(window.confirm("Are you srue, This can NOT be undo")){
        try {
            const res = await axios.delete(`api/profile/user`)
            console.log(res)
            dispatch({type:CLEAR_PROFILE})
            dispatch({type:DELETE_ACCOUNT})        
            dispatch(setAlert("Your account has been permanantly delete","success"))
        } catch (err) {
            
    
            dispatch({
                type: PROFILE_ERROR,
                payload:{msg:err.response.statusText,status:err.response.status}
            })
        }
    }
   
} 