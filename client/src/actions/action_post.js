import axios from "axios"
import { setAlert } from "./action_alert"
import { POST } from "./type"
const { GET_POSTS, POST_ERROR, UPDATE_LIKES, DELETE_POSTS, ADD_POSTS,GET_POST ,ADD_COMMENT,DELETE_COMMENT} = POST

export const getPosts = () => async dispatch => {
	try {
		const res = await axios.get("/api/posts")
		dispatch({
			type: GET_POSTS,
			payload: res.data
		})
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}

export const addLike = postId => async dispatch => {
	try {
		const res = await axios.put(`/api/posts/like/${postId}`)
		dispatch({
			type: UPDATE_LIKES,
			payload: { id: postId, likes: res.data }
		})
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}

export const removeLike = postId => async dispatch => {
	try {
		const res = await axios.put(`/api/posts/unlike/${postId}`)
		dispatch({
			type: UPDATE_LIKES,
			payload: { id: postId, likes: res.data }
		})
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}

export const deletePost = postId => async dispatch => {
	try {
		 await axios.delete(`/api/posts/${postId}`)
		dispatch({
			type: DELETE_POSTS,
			payload: { id: postId }
		})
		dispatch(setAlert("Post Removed", "success"))
	} catch (error) {
        console.log(error)
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}

export const addPost = formData => async dispatch => {
	const cogfig = {
		headers: {
			"Content-Type": "application/json"
		}
	}
	try {
		const res = await axios.post(`/api/posts/`, formData, cogfig)
		dispatch({
			type: ADD_POSTS,
			payload: res.data
		})
		dispatch(setAlert("Post Created", "success"))
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}


export const getPost = (id) => async dispatch => {
	try {
		const res = await axios.get(`/api/posts/${id}`)
		dispatch({
			type: GET_POST,
			payload: res.data
		})
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}



export const addComment = (postId,formData) => async dispatch => {
	const cogfig = {
		headers: {
			"Content-Type": "application/json"
		}
    }
	try {
		const res = await axios.put(`/api/posts/comment/${postId}`, formData, cogfig)
		dispatch({
			type: ADD_COMMENT,
			payload: res.data
		})
		dispatch(setAlert("Comment Added", "success"))
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}



export const deleteComment = (postId,commentId) => async dispatch => {
	
	try {
		 await axios.delete(`/api/posts/comment/${postId}/${commentId}`)
		dispatch({
			type: DELETE_COMMENT,
			payload:{id:commentId}
		})
		dispatch(setAlert("Comment Removed", "success"))
	} catch (error) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: error.response.statusText, status: error.response.status }
		})
	}
}

