import { ALERT } from "./type"
import uuid from "uuid"

export const setAlert = (msg, alertType) => dispatch => {
	const id = uuid.v4
	 dispatch({
		type: ALERT.SET_ALERT,
		payload: {
			id,
			msg,
			alertType
		}
	})

	setTimeout(()=>dispatch({type:ALERT.REMOVE_ALERT,payload:id}),5000)
}
