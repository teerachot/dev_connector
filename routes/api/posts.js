const express = require("express")
const router = express.Router()
const auth = require("../../middleware/auth")
const Post = require("../../model/Post")
const Profile = require("../../model/Profile")
const User = require("../../model/User")
const { check, validationResult } = require("express-validator/check")

// @ router Post /api/posts/
// @ desc  Add posts
// @ access Private

router.post(
	"/",
	[
		check("text", "Text is required")
			.not()
			.isEmpty()
	],
	auth,
	async (req, res) => {
		const error = validationResult(req)
		if (!error.isEmpty()) {
			res.status(400).json({ error: error.array() })
		}
		try {
			const user = await User.findById(req.user.id).select("-password")
			const newPost = {
				text: req.body.text,
				name: user.name,
				avatar: user.avatar,
				user: req.user.id
			}
			const post = new Post(newPost)
			await post.save()
			res.json(post)
		} catch (error) {
			console.log("error", error)
			res.status(500).send("server error")
		}
	}
)

// @ router Get /api/posts/
// @ desc  Get all posts
// @ access Private

router.get("/", auth, async (req, res) => {
	try {
		const posts = await Post.find().sort({ date: -1 })
		res.json(posts)
	} catch (error) {
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router Get /api/posts/:post_id
// @ desc  Get posts by post_id
// @ access Private

router.get("/:post_id", auth, async (req, res) => {
	try {
		const posts = await Post.findById(req.params.post_id)
		if (!posts) {
			return res.status(404).json({ msg: "Post not found" })
		}
		res.json(posts)
	} catch (error) {
		if (error.kind === "ObjectId") {
			return res.status(404).json({ msg: "Post not found" })
		}
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router DELETE /api/posts/:post_id
// @ desc  DELETE posts by post_id
// @ access Private

router.delete("/:post_id", auth, async (req, res) => {
	try {
		const post = await Post.findById(req.params.post_id)
		if (!post) {
			return res.status(404).json({ msg: "Post not found" })
		}
		if (post.user.toString() !== req.user.id) {
			return res.status(400).json({ msg: "User not authorized" })
		}
		await post.remove()
		res.json({ msg: "Post removed" })
	} catch (error) {
		if (error.kind === "ObjectId") {
			return res.status(404).json({ msg: "Post not found" })
		}
		console.log("error", error)
		res.status(500).send("server error")
	}
})


// @ router Put /api/posts/like/:post_id
// @ desc  ADD like on posts
// @ access Private
router.put('/like/:post_id',auth,async (req,res)=>{
    try {
        const post = await Post.findById(req.params.post_id)
        if (!post) {
			return res.status(404).json({ msg: "Post not found" })
		}
        // check if user like has been already
        if(post.likes.filter(like=>like.user.toString() === req.user.id).length > 0){
            return res.status(400).json({msg:"Post already like "})
        }
        post.likes.unshift({user:req.user.id})
        await post.save()
        res.json(post.likes)
    } catch (error) {
        if (error.kind === "ObjectId") {
			return res.status(404).json({ msg: "Post not found" })
		}
        console.log("error", error)
			res.status(500).send("server error")
    }
})

// @ router Put /api/posts/unlike/:post_id
// @ desc   Unlike on posts
// @ access Private
router.put('/unlike/:post_id',auth,async (req,res)=>{
    try {
        const post = await Post.findById(req.params.post_id)
        if (!post) {
			return res.status(404).json({ msg: "Post not found" })
		}
        // check if user like has been already
        if(post.likes.filter(like=>like.user.toString() === req.user.id).length === 0){
            return res.status(400).json({msg:"Post not like post yet"})
        }
        const removeIndex = post.likes.map(like=>like.user.toString()).indexOf(req.user.id) 
        post.likes.splice(removeIndex,1)
        await post.save()
        res.json(post.likes)
    } catch (error) {
        if (error.kind === "ObjectId") {
			return res.status(404).json({ msg: "Post not found" })
		}
        console.log("error", error)
			res.status(500).send("server error")
    }
})



// @ router Put /api/posts/comment/:post_id
// @ desc  ADD comment on posts
// @ access Private

router.put(
	"/comment/:post_id",
	[
		check("text", "Text is required")
			.not()
			.isEmpty()
	],
	auth,
	async (req, res) => {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({error:errors.array()})
        } 
		try {
			const post = await Post.findById(req.params.post_id)
			if (!post) {
				return res.status(404).json({ msg: "Post not found" })
            }
            const user = await User.findById(req.user.id).select("-password")
			const newComment = {
                user:req.user.id,
                text:req.body.text,
                name:user.name,
                avatar:user.avatar
            }
            post.comments.unshift(newComment)
            await post.save()
            res.json(post.comments)
		} catch (error) {
			if (error.kind === "ObjectId") {
				return res.status(404).json({ msg: "Post not found" })
			}
			console.log("error", error)
			res.status(500).send("server error")
		}
	}
)

// @ router Delete /api/posts/comment/:post_id/:comment_id
// @ desc  Delete comment on posts
// @ access Private

router.delete('/comment/:post_id/:comment_id',auth,async (req,res)=>{
    try {
        const post = await Post.findById(req.params.post_id)
		if (!post) {
			return res.status(404).json({ msg: "Post not found" })
        }
        const comment = post.comments.filter(comment=>comment.id===req.params.comment_id)
       
        if(comment.length===0){
            return res.status(400).json({msg:"comment is not exist"})
        }
        if(comment[0].user.toString() !== req.user.id){
             return res.status(400).json({ msg: "User not authorized" })
        }
        const removeIndex = post.comments.map(comments=>comments.user.toString()).indexOf(req.user.id) 
        post.comments.splice(removeIndex,1)
        await post.save()
        res.json(post.comments)
    } catch (error) {
        if (error.kind === "ObjectId") {
            return res.status(404).json({ msg: "Post not found" })
        }
        console.log("error", error)
        res.status(500).send("server error")
    }
})


module.exports = router
