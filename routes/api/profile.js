const express = require("express")
const router = express.Router()
const auth = require("../../middleware/auth")
const User = require("../../model/User")
const { check, validationResult } = require("express-validator/check")
const Profile = require("../../model/Profile")
const Post = require("../../model/Post")
const request = require('request')
const config = require("config")

// @ router Get /api/profile/me
// @ desc   get current user profile
// @ access Private

router.get("/me", auth, async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user.id }).populate(
			"user",
			["name", "avatar"]
		)
		if (!profile) {
			res.status(400).json({ msg: "There is no profile for this user" })
		}
		res.json(profile)
	} catch (error) {
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router Post /api/profile/me
// @ desc   Create and Update current user profile
// @ access Private

router.post(
	"/me",
	[
		check("status", "Status is required")
			.not()
			.isEmpty(),
		check("skills", "Skill is required")
			.not()
			.isEmpty()
	],
	auth,
	async (req, res) => {
		const error = await validationResult(req)
		if (!error.isEmpty()) {
			return res.status(400).json({ error: error.array() })
		}
		let {
			company,
			website,
			location,
			status,
			skills,
			bio,
			githubusername,
			youtube,
			facebook,
			linkedin,
			twitter,
			instagram
		} = req.body

		const profileFields = {}
		profileFields.user = req.user.id
		if (company) profileFields.company = company
		if (website) profileFields.website = website
		if (location) profileFields.location = location
		if (status) profileFields.status = status
		if (githubusername) profileFields.githubusername = githubusername
		if (bio) profileFields.bio = bio
		if (skills) {
			profileFields.skills = skills.split(",").map(skill => skill.trim())
		}
		profileFields.social = {}
		if (youtube) profileFields.social.youtube = youtube
		if (facebook) profileFields.social.facebook = facebook
		if (linkedin) profileFields.social.linkedin = linkedin
		if (twitter) profileFields.social.twitter = twitter
		if (instagram) profileFields.social.instagram = instagram
		try {
			let profile = await Profile.findOne({ user: req.user.id })
			if (profile) {
				profile = await Profile.findOneAndUpdate(
					{ user: req.user.id },
					{ $set: profileFields },
					{ new: true }
				)
				return res.json(profile)
			}
			profile = new Profile(profileFields)
			profile = await profile.save()
			res.json(profile)
		} catch (error) {
			console.log("error", error)
			res.status(500).send("server error")
		}
	}
)

// @ router Get /api/profile/
// @ desc   Get all profile
// @ access Public
router.get("/", async (req, res) => {
	try {
		const profiles = await Profile.find().populate("user", ["name", "avatar"])
		res.json(profiles)
	} catch (error) {
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router Get /api/profile/user/:user_id
// @ desc   Get profile by id
// @ access Public
router.get("/user/:user_id", async (req, res) => {
	try {
		const profile = await Profile.findOne({
			user: req.params.user_id
		}).populate("user", ["name", "avatar"])
		if (!profile) {
			return res.status(404).json({ msg: "Profile not found" })
		}
		res.json(profile)
	} catch (error) {
		console.log("error", error)
		if (error.kind === "ObjectId") {
			return res.status(404).json({ msg: "Profile not found" })
		}
		res.status(500).send("server error")
	}
})

// @ router Delete /api/profile/user
// @ desc   Delete user profile, user & post
// @ access Private
router.delete("/user", auth, async (req, res) => {
	try {
		// to do remove post user
        await Post.deleteMany({ user: req.user.id })
		// remove profile
		await Profile.findOneAndRemove({ user: req.user.id })
		// remove user
		await User.findOneAndRemove({ user: req.user.id })
		res.json({ msg: "User Deleted" })
	} catch (error) {
		console.log("error", error)
		if (error.kind === "ObjectId") {
			return res.status(400).json({ msg: "Profile not found" })
		}
		res.status(500).send("server error")
	}
})

// @ router PUT /api/profile/experience
// @ desc   PUT add experience
// @ access Private

router.put(
	"/experience",
	[
		check("title", "Title is required")
			.not()
			.isEmpty(),
		check("company", "Company is required")
			.not()
			.isEmpty(),
		check("from", "From is required")
			.not()
			.isEmpty()
	],
	auth,
	async (req, res) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			res.status(400).json({ error: errors.array() })
		}
		const {
			title,
			company,
			location,
			from,
			to,
			current,
			description
		} = req.body
		const newExp = {
			title,
			company,
			location,
			from,
			to,
			current,
			description
		}
		try {
			const profile = await Profile.findOne({ user: req.user.id })
			profile.experience.unshift(newExp)
			await profile.save()
			res.json(profile)
		} catch (error) {
			console.log("error", error)
			res.status(500).send("server error")
		}
	}
)

// @ router DELETE /api/profile/experience/:exp_id
// @ desc   DELETE experience
// @ access Private

router.delete("/experience/:exp_id", auth, async (req, res) => {
	try {
        const profile = await Profile.findOne({ user: req.user.id })
        const experience = profile.experience.filter(exp=>exp.id===req.params.exp_id)
        if(experience.length === 0){
            return res.status(400).json({msg:"experience is not exist"})
        }
		const removeIndex = profile.experience
			.map(value => value.id)
			.indexOf(req.params.exp_id)
		profile.experience.splice(removeIndex, 1)
		await profile.save()
		res.json(profile)
	} catch (error) {
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router PUT /api/profile/education
// @ desc   PUT add education
// @ access Private

router.put(
	"/education",
	[
		check("school", "School is required")
			.not()
			.isEmpty(),
		check("degree", "Degree is required")
			.not()
			.isEmpty(),
		check("from", "From is required")
			.not()
			.isEmpty(),
		check("fieldofstudy", "Fieldofstudy is required")
			.not()
			.isEmpty()
	],
	auth,
	async (req, res) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			res.status(400).json({ error: errors.array() })
		}

		const {
			school,
			degree,
			fieldofstudy,
			from,
			to,
			current,
			description
		} = req.body
		const newEdu = {
			school,
			degree,
			fieldofstudy,
			from,
			to,
			current,
			description
		}
		try {
			const profile = await Profile.findOne({ user: req.user.id })
			profile.education.unshift(newEdu)
			await profile.save()
			res.json(profile)
		} catch (error) {
			console.log("error", error)
			res.status(500).send("server error")
		}
	}
)

// @ router DELETE /api/profile/education/:edu_id
// @ desc   DELETE education
// @ access Private

router.delete("/education/:edu_id", auth, async (req, res) => {
	try {
        const profile = await Profile.findOne({ user: req.user.id })
        const education = profile.education.filter(edu=>edu.id===req.params.edu_id)
        if(education.length === 0){
            return res.status(400).json({msg:"education is not exist"})
        }
		const removeIndex = profile.education
			.map(value => value.id)
			.indexOf(req.params.edu_id)
		profile.education.splice(removeIndex, 1)
		await profile.save()
		res.json(profile)
	} catch (error) {
		console.log("error", error)
		res.status(500).send("server error")
	}
})

// @ router GET /api/profile/github/:username
// @ desc   GET github
// @ access Private

router.get("/github/:username",async (req,res)=>{
    try {
        const option = {
            uri:`https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&client_id=${config.get('gitHubClientId')}&client_secret=${config.get('gitHubClientSecret')}`,
            method:'GET',
            headers:{'user-agent':'node.js'}
        }
        request(option,(error,respones,body)=>{
            if(error){
                console.log(error)
            }
            if(respones.statusCode !== 200){
                res.status(400).json({msg:"No Github profile found"})
            }
            res.json(respones)
        })
    } catch (error) {
        console.log("error", error)
		res.status(500).send("server error")
    }
})

module.exports = router
