const mongoose = require("mongoose")
const config = require("config")
const db = config.get("mongoURI")

const ConnectedDB = async () => {
	try {
		await mongoose.connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false
		})
		console.log("Database Connected....")
	} catch (error) {
		console.log("ERROR:", error)
		process.exit(1)
	}
}

module.exports = ConnectedDB
