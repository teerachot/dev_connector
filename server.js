const express = require('express')
const ConnectDB = require('./config/db')
const app =  express()
const user = require('./routes/api/user')
const auth = require('./routes/api/auth')

const posts = require('./routes/api/posts')

const profile = require('./routes/api/profile')

const path = require('path')

// Connect Database
ConnectDB()

//init middleware 
app.use(express.json({extended:false}))
// app.get('/', (req,res)=> res.send('API Running '))

// Default routes
app.use("/api/user",user)
app.use("/api/auth",auth)
app.use("/api/profile",profile)
app.use("/api/posts",posts)

// serve static assets in production
if(process.env.NODE_ENV === "production"){
    // set static folder
    app.use(express.static('client/build'))
    app.get("*",(req,res)=>{
        res.sendFile(path.resolve(__dirname,'client','build','index.html'))
    })
}

const PORT = process.env.PORT || 5000

app.listen(PORT,()=>console.log(`server started on port ${PORT}`))